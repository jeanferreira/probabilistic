package br.com.jeanferreira.probabilistic;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText lqi;
    private EditText n;
    private EditText Fant;
    private EditText h;
    private EditText fqi;
    private TextView resultado;

    private EditText i;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    public void calcularModa(View view){
        lqi = (EditText) findViewById(R.id.lmo);
        EditText delta1 = (EditText) findViewById(R.id.delta1);
        EditText delta2 = (EditText) findViewById(R.id.delta2);
        h = (EditText) findViewById(R.id.hModa);

        if(lqi.length() > 0 && delta1.length() > 0 && delta2.length() > 0 && h.length() > 0){
            float Mo = Float.parseFloat(lqi.getText().toString()) + ((Float.parseFloat(delta1.getText().toString())/(Float.parseFloat(delta2.getText().toString())+Float.parseFloat(delta1.getText().toString())))*Float.parseFloat(h.getText().toString()));
            resultado = (TextView) findViewById(R.id.textoResultadoModa);
            resultado.setText(Float.toString(Mo));
        } else {
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularMedia(View view){
        EditText xifi = (EditText) findViewById(R.id.xifiMedia);
        EditText fi = (EditText) findViewById(R.id.fiMedia);

        if(xifi.length() > 0 && fi.length() > 0){
            float x = Float.parseFloat(xifi.getText().toString())/Float.parseFloat(fi.getText().toString());
            resultado = (TextView) findViewById(R.id.textoResultadoMedia);
            resultado.setText(Float.toString(x));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularMediana(View view){
        lqi = (EditText) findViewById(R.id.lmd);
        n = (EditText) findViewById(R.id.nMediana);
        Fant = (EditText) findViewById(R.id.fantMediana);
        h = (EditText) findViewById(R.id.hMediana);
        fqi = (EditText) findViewById(R.id.fmd);

        if(lqi.length() > 0 && n.length() > 0 && Fant.length() > 0 && h.length() > 0 && fqi.length() > 0) {
            float Md = Float.parseFloat(lqi.getText().toString()) + (((Float.parseFloat(n.getText().toString()) / 2) - Float.parseFloat(Fant.getText().toString())) * Float.parseFloat(h.getText().toString()) / Float.parseFloat(fqi.getText().toString()));
            resultado = (TextView) findViewById(R.id.textoResultadoMediana);
            resultado.setText(Float.toString(Md));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularVariancia(View view){
        EditText xi2fi = (EditText) findViewById(R.id.xi2fi);
        EditText xifi = (EditText) findViewById(R.id.xifiVariancia);
        EditText fi = (EditText) findViewById(R.id.fi);

        if(xi2fi.length() > 0 && xifi.length() > 0 && fi.length() > 0){
            float s2 = (Float.parseFloat(xi2fi.getText().toString())-(Float.parseFloat(xifi.getText().toString())*Float.parseFloat(xifi.getText().toString())/Float.parseFloat(fi.getText().toString())))/Float.parseFloat(fi.getText().toString())-1;
            resultado = (TextView) findViewById(R.id.textoResultadoVariancia);
            resultado.setText(Float.toString(s2));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularDesvioPadrao(View view){
        EditText s2 = (EditText) findViewById(R.id.s2);

        if(s2.length() > 0) {
            double s = Math.sqrt(Double.parseDouble(s2.getText().toString()));
            resultado = findViewById(R.id.textoResultadoDesvioPadrao);
            resultado.setText(Double.toString(s));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularCoeficienteVariacao(View view){
        EditText s = (EditText) findViewById(R.id.s);
        EditText x = (EditText) findViewById(R.id.x);

        if(s.length() > 0 && x.length() > 0){
            float cv = Float.parseFloat(s.getText().toString())/Float.parseFloat(x.getText().toString())*100;
            resultado = findViewById(R.id.textoResultadoCoeficienteVariacao);
            resultado.setText(Float.toString(cv));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularQuartil(View view){
        lqi = (EditText) findViewById(R.id.lqi);
        n = (EditText) findViewById(R.id.n);
        Fant = (EditText) findViewById(R.id.Fant);
        h = (EditText) findViewById(R.id.h);
        fqi = (EditText) findViewById(R.id.fqi);

        if(lqi.length() > 0 && n.length() > 0 && Fant.length() > 0 && h.length() > 0 && fqi.length() > 0) {
            float Qi = Float.parseFloat(lqi.getText().toString()) + (((Float.parseFloat(n.getText().toString()) / 4) - Float.parseFloat(Fant.getText().toString())) * Float.parseFloat(h.getText().toString()) / Float.parseFloat(fqi.getText().toString()));
            resultado = (TextView) findViewById(R.id.textoResultadoQuartil);
            resultado.setText(Float.toString(Qi));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularDecil(View view){
        lqi = (EditText) findViewById(R.id.ldi);
        i = (EditText) findViewById(R.id.iDecil);
        n = (EditText) findViewById(R.id.nDecil);
        Fant = (EditText) findViewById(R.id.FantDecil);
        h = (EditText) findViewById(R.id.hDecil);
        fqi = (EditText) findViewById(R.id.fdi);

        if(lqi.length() > 0 && lqi.length() > 0 && n.length() > 0 && Fant.length() > 0 && h.length() > 0 && fqi.length() > 0) {
            float Di = Float.parseFloat(lqi.getText().toString()) + (((Float.parseFloat(i.getText().toString()) * Float.parseFloat(n.getText().toString()) / 10) - Float.parseFloat(Fant.getText().toString())) * Float.parseFloat(h.getText().toString()) / Float.parseFloat(fqi.getText().toString()));
            resultado = (TextView) findViewById(R.id.textoResultadoDecil);
            resultado.setText(Float.toString(Di));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void calcularPercentil(View view){
        lqi = (EditText) findViewById(R.id.lpi);
        i = (EditText) findViewById(R.id.iPercentil);
        n = (EditText) findViewById(R.id.nPercentil);
        Fant = (EditText) findViewById(R.id.FantPercentil);
        h = (EditText) findViewById(R.id.hPercentil);
        fqi = (EditText) findViewById(R.id.fpi);

        if(lqi.length() > 0 && lqi.length() > 0 && n.length() > 0 && Fant.length() > 0 && h.length() > 0 && fqi.length() > 0) {
            float Pi = Float.parseFloat(lqi.getText().toString()) + (((Float.parseFloat(i.getText().toString()) * Float.parseFloat(n.getText().toString()) / 10) - Float.parseFloat(Fant.getText().toString())) * Float.parseFloat(h.getText().toString()) / Float.parseFloat(fqi.getText().toString()));
            resultado = (TextView) findViewById(R.id.textoResultadoPercentil);
            resultado.setText(Float.toString(Pi));
        } else{
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sobre) {

            final Intent i = new Intent(this,Sobre.class);
            startActivity(i);

            return true;
        }

        if(id == R.id.action_Politica){

            final  Intent i = new Intent(this, Politica.class);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab1medidas_tendencia, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    Tab1MedidasTendencia tab1 = new Tab1MedidasTendencia();
                    return tab1;
                case 1:
                    Tab2MedidasVariabilidade tab2 = new Tab2MedidasVariabilidade();
                    return tab2;
                case 2:
                    Tab3Quantis tab3 = new Tab3Quantis();
                    return tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
